declare_PID_Component(
    APPLICATION
    NAME hri-teaching-app
    DIRECTORY hri_teaching_app
    RUNTIME_RESOURCES bazar_config bazar_log
    DEPEND
        rkcl-bazar-robot/rkcl-bazar-robot
        rkcl-otg-reflexxes/rkcl-otg-reflexxes
        pid-rpath/rpathlib
        pid-os-utilities/pid-signal-manager
        rkcl-app-utility/rkcl-app-utility
        rkcl-filters/rkcl-filters
        openpose-kinect2/skeleton3d
)
