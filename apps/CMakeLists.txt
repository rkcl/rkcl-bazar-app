
include(collision_avoidance_app/collision_avoidance_app.cmake)
include(simple_simu_app/simple_simu_app.cmake)
if(REAL_ROBOT)
    include(collision_avoidance_one_arm_app/collision_avoidance_one_arm_app.cmake)
    include(hri_comanipulation_app/hri_comanipulation_app.cmake)
    include(hri_comanipulation_teaching_app/hri_comanipulation_teaching_app.cmake)
    include(hri_teaching_app/hri_teaching_app.cmake)
endif()


