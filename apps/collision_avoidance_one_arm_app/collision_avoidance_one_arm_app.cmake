declare_PID_Component(
    APPLICATION
    NAME collision-avoidance-one-arm-app
    DIRECTORY collision_avoidance_one_arm_app
    RUNTIME_RESOURCES bazar_config bazar_log
    DEPEND
        rkcl-bazar-robot/rkcl-bazar-robot
        rkcl-otg-reflexxes/rkcl-otg-reflexxes
        pid-rpath/rpathlib
        pid-os-utilities/pid-signal-manager
        rkcl-app-utility/rkcl-app-utility
        rkcl-driver-vrep/rkcl-driver-vrep
        openpose-kinect2/skeleton3d
)
