/**
 * @file main.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Application that shows the collision avoidance strategy, on simulation
 * @date 13-03-2020
 * License: CeCILL
 */
#include <rkcl/robots/bazar.h>
#include <rkcl/drivers/vrep_driver.h>
#include <rkcl/processors/vrep_visualization.h>
#include <rkcl/processors/otg_reflexxes.h>
#include <rkcl/processors/app_utility.h>
#include <yaml-cpp/yaml.h>
#include <pid/rpath.h>
#include <pid/signal_manager.h>

#include <iostream>
#include <vector>
#include <thread>
#include <unistd.h>

int main()
{
    rkcl::DriverFactory::add<rkcl::VREPMainDriver>("vrep_main");
    rkcl::DriverFactory::add<rkcl::VREPJointDriver>("vrep_joint");

    rkcl::QPSolverFactory::add<rkcl::OSQPSolver>("osqp");

    auto conf = YAML::LoadFile(PID_PATH("bazar_config/collision_avoidance_init_config.yaml"));
    auto app = rkcl::AppUtility::create<rkcl::ForwardKinematicsRBDyn, rkcl::JointSpaceOTGReflexxes>(conf);
    app.add<rkcl::CollisionAvoidanceSCH>();

    rkcl::TaskSpaceOTGReflexxes task_space_otg(
        app.getRobot(), app.getTaskSpaceController().getControlTimeStep());

    if (not app.init())
    {
        throw std::runtime_error("Cannot initialize the application");
    }

    rkcl::VREPVisualization vrep_visu(app.getRobot());
    vrep_visu.init();

    app.addDefaultLogging();

    for (size_t i = 0; i < app.getRobot().controlPointCount(); ++i)
    {
        app.getTaskSpaceLogger().log(app.getRobot().getControlPoint(i)->name + " task velocity command", app.getTaskSpaceController().getControlPointTaskVelocityCommand(i));
    }
    for (size_t i = 0; i < app.getRobot().jointGroupCount(); ++i)
    {
        app.getTaskSpaceLogger().log(app.getRobot().getJointGroup(i)->name + " task contribution", &(app.getInverseKinematicsController().getJointGroupContribution(i)), 1);
    }

    bool stop = false;
    bool done = false;

    pid::SignalManager::registerCallback(pid::SignalManager::Interrupt, "stop", [&stop](int) { stop = true; });

    try
    {
        std::cout << "Starting control loop \n";
        app.configureTask(0);
        task_space_otg.reset();
        while (not stop and not done)
        {
            bool ok = app.runControlLoop(
                [&] {
                    if (app.isTaskSpaceControlEnabled())
                        return task_space_otg();
                    else
                        return true;
                },
                [&] {
                    if (app.isTaskSpaceControlEnabled())
                        return vrep_visu();
                    else
                        return true;
                });

            if (ok)
            {
                done = true;
                if (app.isTaskSpaceControlEnabled())
                {
                    done &= (app.getTaskSpaceController().getControlPointsPoseErrorGoalNormPosition() < 0.01);
                }
                if (app.isJointSpaceControlEnabled())
                {
                    for (const auto& joint_space_otg : app.getJointSpaceOTGs())
                    {
                        if (joint_space_otg->getJointGroup()->control_space == rkcl::JointGroup::ControlSpace::JointSpace)
                        {
                            if (joint_space_otg->getControlMode() == rkcl::JointSpaceOTG::ControlMode::Position)
                            {
                                auto joint_group_error_pos_goal = joint_space_otg->getJointGroup()->selection_matrix * (joint_space_otg->getJointGroup()->goal.position - joint_space_otg->getJointGroup()->state.position);
                                done &= (joint_group_error_pos_goal.norm() < 0.001);
                            }
                            else if (joint_space_otg->getControlMode() == rkcl::JointSpaceOTG::ControlMode::Velocity)
                            {
                                auto joint_group_error_vel_goal = joint_space_otg->getJointGroup()->selection_matrix * (joint_space_otg->getJointGroup()->goal.velocity - joint_space_otg->getJointGroup()->state.velocity);
                                done &= (joint_group_error_vel_goal.norm() < 1e-10);
                            }
                        }
                    }
                }
            }
            else
            {
                throw std::runtime_error("Something wrong happened in the control loop, aborting");
            }
            if (done)
            {
                done = false;
                std::cout << "Task completed, moving to the next one" << std::endl;
                done = not app.nextTask();
                task_space_otg.reset();
            }
        }
        if (stop)
            throw std::runtime_error("Caught user interruption, aborting");

        std::cout << "All tasks completed" << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    pid::SignalManager::unregisterCallback(pid::SignalManager::Interrupt, "stop");

    std::cout << "Ending the application" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(2));
    rkcl::VREPMainDriver::notifyStop();

    if (app.end())
        std::cout << "Application ended properly" << std::endl;
    else
        std::cout << "Application ended badly" << std::endl;
    return 0;
}
